import 'dart:io';

import 'package:dio/dio.dart';

const baseUrl = 'https://api.unsplash.com/';
const apiKey = '5dGgg9ovcxQVfQvyjoUoUE4L6vmmW5Z7-6Uv5kIlzYo';

class ApiProvider {
  Dio get dio => _dio();

  Dio _dio() {
    var dio = Dio(options());
    return dio;
  }

  BaseOptions options() {
    var options = BaseOptions(
      baseUrl: baseUrl,
      contentType: 'application/json',
      responseType: ResponseType.plain,
      headers: {
        HttpHeaders.authorizationHeader: 'Client-ID $apiKey',
      },
    );
    return options;
  }
}
